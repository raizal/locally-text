package com.raizint.locally.dao;

import java.lang.reflect.Field;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.raizint.locally.etc.Constant;
import com.raizint.locally.etc.Utils;

public class DeviceInfo {

	public static DeviceInfo getInstance(Context context) {
		String[] operators = null;
		if (Utils.getSettingStringValue(context, Utils.UID).equals("") || Utils.getSettingStringValue(context, Utils.OS).equals("")) {
			Utils.setSettingValue(context, Utils.UID, Constant.getUniquePsuedoID());
			Utils.setSettingValue(context, Utils.OS, getSDKCodeName());
			Utils.setSettingValue(context, Utils.NAME, android.os.Build.MANUFACTURER + " " + android.os.Build.PRODUCT);
			Utils.setSettingValue(context, Utils.MODEL, android.os.Build.MANUFACTURER + " " + android.os.Build.PRODUCT);		
		}
		
		operators = new String[Utils.getPhoneCount()];

		if (operators.length == 1) {
			TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			operators[0] = manager.getNetworkOperatorName();
		} else {
			for (int i = 0; i < operators.length; i++) {
				operators[i] = Utils.getNetworkOperatorName(i);
			}
		}

		return new DeviceInfo(Utils.getSettingStringValue(context, Utils.UID), Utils.getSettingStringValue(context, Utils.NAME), Utils.getSettingStringValue(context, Utils.OS), Utils.getSettingStringValue(context, Utils.MODEL), operators);
	}

	private static String getSDKCodeName() {
		String codeName = "";
		Field[] fields = Build.VERSION_CODES.class.getFields();
		for (Field field : fields) {
			String fieldName = field.getName();
			int fieldValue = -1;

			try {
				fieldValue = field.getInt(new Object());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			if (fieldValue == Build.VERSION.SDK_INT) {
				codeName = fieldName;
			}
		}
		return codeName;
	}

	private String deviceID, deviceName, androidVersion, deviceModel;
	private String[] operators = null;

	public DeviceInfo(String deviceID, String deviceName, String androidVersion, String deviceModel, String[] operators) {
		setAndroidVersion(androidVersion);
		setDeviceID(deviceID);
		setDeviceName(deviceName);
		setDeviceModel(deviceModel);
		this.operators = operators;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String[] getOperators() {
		return operators;
	}

}
