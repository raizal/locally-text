package com.raizint.locally.dao;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.raizint.locally.etc.Utils;
import com.raizint.locally.shared.Contact;

public class DaoContact {

	public static ArrayList<Contact> getContact(Context c) {

		HashMap<String, Contact> contacts = new HashMap<String, Contact>();

		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER };

		Cursor people = c.getContentResolver().query(uri, projection, null, null, null);

		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

		people.moveToFirst();
		do {
			String name = people.getString(indexName);
			String number = people.getString(indexNumber).replaceAll("-", "");
			Utils.sendToUI("CONTACTS : "+ number ,c);
			if (contacts.containsKey(name)) {
				contacts.get(name).addNumber(number);
			} else {
				Contact contact = new Contact(name);
				contact.addNumber(number);
				contacts.put(name, contact);
			}			
		} while (people.moveToNext());
		people.close();			
		
		return new ArrayList<Contact>(contacts.values());
	}
}
