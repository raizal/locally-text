package com.raizint.locally.dao;

import java.util.ArrayList;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;

import com.raizint.locally.db.Constant;
import com.raizint.locally.etc.Utils;
import com.raizint.locally.shared.SMS;

public class DaoSMS {

	public static final String SENT_ACTION_FILTER = "com.raizint.locally.sms.sending";
	public static final String DELIVERED_ACTION_FILTER = "com.raizint.locally.sms.delivered";
	public static final String SMS_EXTRA_MESSAGE = "message";
	public static final String SMS_EXTRA_ADDRESS = "address";
	public static final String SMS_EXTRA_MULTIPART = "multipart";
	public static final String SMS_EXTRA_IP = "ip";
	public static final String SMS_EXTRA_TIME = "datetime";

	public static int countMessages(Context context, long time) {
		int result = 0;
		result += countSMS(context, Constant.Inbox, time);
		result += countSMS(context, Constant.Sent, time);
		result += countSMS(context, Constant.Draft, time);
		result += countSMS(context, Constant.Failed, time);
		return result;
	}

	public static ArrayList<SMS> getMessages(Context context, long time) {

		ArrayList<SMS> messages = new ArrayList<SMS>();

		try {
			readSMS(context, messages, Constant.Inbox, SMS.SMS_TYPE_INBOX_READ, time);
			readSMS(context, messages, Constant.Sent, SMS.SMS_TYPE_OUTBOX_SENDING, time);
			readSMS(context, messages, Constant.Draft, SMS.SMS_TYPE_DRAFT, time);
			readSMS(context, messages, Constant.Failed, SMS.SMS_TYPE_OUTBOX_ERROR, time);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.w("SMS COUNT", Integer.valueOf(messages.size()).toString());

		// Collections.sort(messages, new Comparator<SMS>() {
		//
		// @Override
		// public int compare(SMS lhs, SMS rhs) {
		// return (int) (Long.parseLong(rhs.getDatetime()) -
		// Long.parseLong(lhs.getDatetime()));
		// }
		// });
		return messages;
	}

	public static void sendMessage(Context c, String address, String message, String time, String ip) {
		sendMessage(c, address, message, time, ip, 0);
	}

	public static void sendMessage(Context c, String address, String message, String time, String ip, int sim) {

		SmsManager sms = SmsManager.getDefault();

		if (message.length() > 160) {
			ArrayList<String> parts = sms.divideMessage(message);
			ArrayList<PendingIntent> sentsPi = new ArrayList<PendingIntent>(), deliveredsPi = new ArrayList<PendingIntent>();

			for (String msg : parts) {

				Bundle data = new Bundle();
				data.putString(SMS_EXTRA_ADDRESS, address);
				data.putString(SMS_EXTRA_MESSAGE, msg);
				data.putString(SMS_EXTRA_IP, ip);
				data.putString(SMS_EXTRA_TIME, time);
				data.putBoolean(SMS_EXTRA_MULTIPART, true);

				Intent sentIntent = new Intent(SENT_ACTION_FILTER);
				sentIntent.putExtras(data);

				Intent deliveredIntent = new Intent(DELIVERED_ACTION_FILTER);
				deliveredIntent.putExtras(data);

				sentsPi.add(createUnique(c, sentIntent));
				deliveredsPi.add(createUnique(c, deliveredIntent));
			}
			if (Utils.getPhoneCount() > 1) {
				// if (sim == 0) {
				// sms.sendMultipartTextMessage(address, null, parts, sentsPi,
				// deliveredsPi);
				// } else {
				Utils.SendSMSMultiPart(address, message, sim, sentsPi, deliveredsPi);
				// }
			} else {
				sms.sendMultipartTextMessage(address, null, parts, sentsPi, deliveredsPi);
			}
		} else {

			Bundle data = new Bundle();
			data.putString(SMS_EXTRA_ADDRESS, address);
			data.putString(SMS_EXTRA_MESSAGE, message);
			data.putString(SMS_EXTRA_IP, ip);
			data.putString(SMS_EXTRA_TIME, time);
			data.putBoolean(SMS_EXTRA_MULTIPART, false);

			Intent sentIntent = new Intent(SENT_ACTION_FILTER);
			sentIntent.putExtras(data);

			Intent deliveredIntent = new Intent(DELIVERED_ACTION_FILTER);
			deliveredIntent.putExtras(data);

			PendingIntent sentPi = createUnique(c, sentIntent);

			PendingIntent deliveredPi = createUnique(c, deliveredIntent);

			if (Utils.getPhoneCount() > 1) {
				// if (sim == 0) {
				// sms.sendTextMessage(address, null, message, sentPi,
				// deliveredPi);
				// } else {
				ArrayList<String> parts = sms.divideMessage(message);
				ArrayList<PendingIntent> sentsPi = new ArrayList<PendingIntent>(), deliveredsPi = new ArrayList<PendingIntent>();

				for (String msg : parts) {

					data = new Bundle();
					data.putString(SMS_EXTRA_ADDRESS, address);
					data.putString(SMS_EXTRA_MESSAGE, msg);
					data.putString(SMS_EXTRA_IP, ip);
					data.putString(SMS_EXTRA_TIME, time);
					data.putBoolean(SMS_EXTRA_MULTIPART, true);

					sentIntent = new Intent(SENT_ACTION_FILTER);
					sentIntent.putExtras(data);

					deliveredIntent = new Intent(DELIVERED_ACTION_FILTER);
					deliveredIntent.putExtras(data);

					sentsPi.add(createUnique(c, sentIntent));
					deliveredsPi.add(createUnique(c, deliveredIntent));
				}
				Utils.SendSMSMultiPart(address, message, sim, sentsPi, deliveredsPi);
				// }
			} else {
				sms.sendTextMessage(address, null, message, sentPi, deliveredPi);
			}
		}
	}

	private static int countSMS(Context context, String uri, long time) {
		Cursor c = context.getContentResolver().query(Uri.parse(uri), new String[] { "count(*)" }, "CAST(date as Integer)>=" + String.valueOf(time), null, null);
		if (c.moveToFirst()) {
			return c.getInt(0);
		}
		c.close();
		return 0;
	}

	private static void readSMS(Context context, ArrayList<SMS> list, String uri, int type, long time) {

		try {
			Cursor c = context.getContentResolver().query(Uri.parse(uri), new String[] { "address", "body", "date" },
			// null,
					"CAST(date as Integer)>=" + String.valueOf(time), null, null);
			if (c.moveToFirst()) {
				do {
					String number = c.getString(0);
					
					number = number.replace("-", "");
			    	number = number.replace(" ", "");
			    	number = number.replace("(", "");
			    	number = number.replace(")", "");
			    	number = number.trim();
					
					SMS sms = new SMS(null,number, c.getString(1), c.getString(2), type);
					list.add(sms);
				} while (c.moveToNext());
			} else {
				Log.d("READ SMS", "NO DATA");
			}
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static PendingIntent createUnique(Context c, Intent sentIntent) {
		int seedSent = (int) (System.currentTimeMillis() + Math.random() * 1000);
		PendingIntent sentPi = PendingIntent.getBroadcast(c, seedSent, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		// if(sentPi == null){
		// sentPi = PendingIntent.getBroadcast(c,
		// seedSent,
		// sentIntent,
		// PendingIntent.FLAG_UPDATE_CURRENT);
		// }else{
		// int newSeedSent = 0;
		// do{
		// newSeedSent = (int) (System.currentTimeMillis() + Math.random() *
		// 1000);
		// }while(seedSent == newSeedSent);
		// sentPi = PendingIntent.getBroadcast(c,
		// newSeedSent,
		// sentIntent,
		// PendingIntent.FLAG_UPDATE_CURRENT);
		// }
		//
		return sentPi;
	}

}
