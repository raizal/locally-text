package com.raizint.locally.dao;

import java.util.List;

import android.database.Cursor;

public interface Dao<T> {
	
	public Cursor get(String where);	
	public long update(T object);
	public long insert(T object);
	public long delete(T object);
	public List<T> getList(String where);
}
