package com.raizint.locally;

import java.net.DatagramPacket;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.internal.ms;
import com.raizint.locally.dao.DeviceInfo;
import com.raizint.locally.etc.PushService;
import com.raizint.locally.etc.Utils;
import com.raizint.locally.shared.Commands;
import com.raizint.locally.shared.Data;

public class AuthActivity extends FragmentActivity {

	private DatagramPacket packet;
	private InterstitialAd mInterstitialAd;

	private boolean ready = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth);

		if (getIntent().getExtras() != null) {
			packet = Utils.getGson().fromJson(getIntent().getStringExtra("data"), DatagramPacket.class);
		}

		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId("ca-app-pub-1770889364344654/6412223170");
		AdRequest adRequest = new AdRequest.Builder().build();
		mInterstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				finish();
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {				
				super.onAdFailedToLoad(errorCode);
			}
			
			@Override
			public void onAdLoaded() {
				if(ready)
					mInterstitialAd.show();
			}

		});
		mInterstitialAd.loadAd(adRequest);
		new AuthDialog().show();
	}

	private class AuthDialog extends Dialog {

		EditText code;

		public AuthDialog() {
			super(AuthActivity.this);
			setTitle("Connecting");
			setCancelable(false);
		}

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			requestWindowFeature(Window.FEATURE_NO_TITLE);

			setContentView(R.layout.auth_dialog);
			code = (EditText) findViewById(R.id.auth_code);

			Button cancel = (Button) findViewById(R.id.cancel);

			cancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					if (PushService.getServer() != null && packet != null) {

						new Thread() {
							public void run() {
								PushService.getServer().reply(packet.getAddress(), Utils.getGson().toJson(new Data(Commands.CONNECT_AUTH_SEND_CANCEL, DeviceInfo.getInstance(AuthActivity.this).getDeviceID(), "")));
							};
						}.start();

					}

					dismiss();
					finish();
				}
			});

			Button submit = (Button) findViewById(R.id.submit);
			submit.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (PushService.getServer() != null && packet != null) {
						final Data data = new Data(Commands.CONNECT_AUTH_SEND, DeviceInfo.getInstance(AuthActivity.this).getDeviceID(), code.getText().toString());
						new Thread() {
							public void run() {
								PushService.getServer().reply(packet.getAddress(), Utils.getGson().toJson(data));
							};
						}.start();
					}
					dismiss();
					if(mInterstitialAd.isLoaded())
						mInterstitialAd.show();
					else
						ready = true;
				}
			});
		}

	}
}
