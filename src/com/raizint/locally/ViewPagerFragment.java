package com.raizint.locally;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.raizint.locally.etc.ClassFinder;
import com.raizint.locally.etc.Utils;

public class ViewPagerFragment extends Fragment {

	private View view;
	private TextView log;

	private static ViewPagerFragment instance;

	public static ViewPagerFragment getInstance() {
		return instance;
	}

	public ViewPagerFragment() {
		instance = this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedIOnstanceState) {
		view = inflater.inflate(R.layout.fragment_fragment1, container, false);
		log = (TextView) view.findViewById(R.id.log);
		view.getContext().registerReceiver(receiver, new IntentFilter("SERVICE.RESULT"));

		// Intent test = new Intent("SERVICE.RESULT");
		// test.putExtra("text", "Testing receiver");
		// view.getContext().sendBroadcast(test);
		//
		writeLog("CHECK API API DUAL SIM : " + Utils.checkapi(inflater.getContext()));
		try {
			writeLog("CLASS LIST TELEPHONY : " + ClassFinder.find("android.telephony").toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		writeLog("CHECK API SMS_MANAGER : \n" + Utils.checkapi(getActivity(), "android.telephony.SmsManager"));
		// writeLog("CHECK SEND SMS SIM 1 : "+Utils.SendSMS("088803559828",
		// "TESTING API 1 ",0));
		// writeLog("CHECK SEND SMS SIM 2 : "+Utils.SendSMS("088803559828",
		// "TESTING API 2",1));
		// writeLog("GET Phone 1 Operator : "+Utils.getNetworkOperatorName(0));
		// writeLog("GET Phone 2 Operator : "+Utils.getNetworkOperatorName(1));
		// writeLog("GET Phone Count : "+Utils.getPhoneCount());
		// writeLog("CHECK API : \n"+Utils.checkapi(getActivity(),
		// "com.android.internal.telephony.MSimIccSmsInterfaceManager"));
		// writeLog("OPERATOR : \n"+Utils.getNetworkOperatorName(getActivity()));

		Button button = (Button) view.findViewById(R.id.button);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				log.setText("");
			}
		});

		return view;
	}

	public void writeLog(Object message) {
		log.setText(log.getText() + message.toString() + "\n");
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("SERVICE.RESULT")) {
				writeLog(intent.getStringExtra("text"));
			}
		}

	};

	public void onResume() {
		if (view != null)
			view.getContext().registerReceiver(receiver, new IntentFilter("SERVICE.RESULT"));
		super.onResume();
	};

	public void onDetach() {
		ViewPagerFragment.getInstance().writeLog("Detaching Receiver");
		view.getContext().unregisterReceiver(receiver);
		super.onDetach();
	};
}
