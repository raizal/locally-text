package com.raizint.locally.etc;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.google.gson.Gson;
import com.raizint.locally.dao.DaoSMS;
import com.raizint.locally.dao.DeviceInfo;
import com.raizint.locally.db.Constant;
import com.raizint.locally.shared.Commands;
import com.raizint.locally.shared.Data;
import com.raizint.locally.shared.Report;
import com.raizint.locally.shared.SMS;

public class SmsListener extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		Log.d("SMSListener", "SIP MASUK");
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
			Bundle bundle = intent.getExtras(); // ---get the SMS message passed
			// in---
			SmsMessage[] msgs = null;
			if (bundle != null) {
				// ---retrieve the SMS message received---
				try {
					Calendar time = Calendar.getInstance();
					String msg_from = "";
					String msgBody = "";
					Object[] pdus = (Object[]) bundle.get("pdus");
					msgs = new SmsMessage[pdus.length];
					for (int i = 0; i < msgs.length; i++) {
						msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
						msg_from = msgs[i].getOriginatingAddress();
						msgBody += msgs[i].getMessageBody();
						time.setTimeInMillis(msgs[i].getTimestampMillis());
					}

					SMS sms = new SMS(null, msg_from, msgBody, String.valueOf(time.getTimeInMillis()), SMS.SMS_TYPE_INBOX_UNREAD);					
					PushService temp = PushService.getInstance();
					if(temp!=null)
						temp.sendSMS(sms);
					Utils.sendToUI("From : " + msg_from + "\n" + msgBody, context);

				} catch (Exception e) {
					Log.d("Exception caught", e.getMessage());
				}
			}

		} else if (intent.getAction().equals(DaoSMS.DELIVERED_ACTION_FILTER)) {

			if (intent.getExtras() != null) {
				String message = intent.getStringExtra(DaoSMS.SMS_EXTRA_MESSAGE);
				String address = intent.getStringExtra(DaoSMS.SMS_EXTRA_ADDRESS);
				String time = intent.getStringExtra(DaoSMS.SMS_EXTRA_TIME);
				final String ip = intent.getStringExtra(DaoSMS.SMS_EXTRA_IP);
				boolean multipart = intent.getBooleanExtra(DaoSMS.SMS_EXTRA_MULTIPART, false);
				Report report = new Report(message, address, time, Report.STATUS_DELIVERED, null);

				ContentResolver cr = context.getContentResolver();
				ContentValues values = new ContentValues();
				values.put("address", address);
				values.put("body", message);
				values.put("date", time);
				cr.insert(Uri.parse(Constant.Sent), values);
				Log.d("INSERT TO PROVIDER", "SIPPPP");

				Data data = new Data(Commands.SMS_SEND_REPORT, DeviceInfo.getInstance(context).getDeviceID(), new Gson().toJson(report));

				final String dataString = new Gson().toJson(data);
				new Thread() {
					public void run() {
						try {
							PushService.getServer().reply(InetAddress.getByName(ip), dataString);
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					};
				}.start();

			}

		} else if (intent.getAction().equals(DaoSMS.SENT_ACTION_FILTER)) {

			String message = intent.getStringExtra(DaoSMS.SMS_EXTRA_MESSAGE);
			String address = intent.getStringExtra(DaoSMS.SMS_EXTRA_ADDRESS);
			String time = intent.getStringExtra(DaoSMS.SMS_EXTRA_TIME);
			final String ip = intent.getStringExtra(DaoSMS.SMS_EXTRA_IP);
			boolean multipart = intent.getBooleanExtra(DaoSMS.SMS_EXTRA_MULTIPART, false);

			String cause = "";
			if (getResultCode() == Activity.RESULT_OK) {
				// Report report = new Report(message, address,
				// Report.STATUS_SENDING,null);
				Report report = new Report(message, address, time, Report.STATUS_DELIVERED, null);

				Data data = new Data(Commands.SMS_SEND_REPORT, DeviceInfo.getInstance(context).getDeviceID(), new Gson().toJson(report));

				final String dataString = new Gson().toJson(data);
				new Thread() {
					public void run() {
						try {
							PushService.getServer().reply(InetAddress.getByName(ip), dataString);
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					};
				}.start();
			} else {
				switch (getResultCode()) {
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					cause = "Transmission failed";
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					cause = "Radio off";
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					cause = "No PDU defined";
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					cause = "No service";
					break;
				}

				Report report = new Report(message, address, time, Report.STATUS_ERROR, cause);

				ContentResolver cr = context.getContentResolver();
				ContentValues values = new ContentValues();
				values.put("address", address);
				values.put("body", message);
				values.put("date", time);
				cr.insert(Uri.parse(Constant.Failed), values);
				Log.d("INSERT TO PROVIDER", "SIPPPP");

				Data data = new Data(Commands.SMS_SEND_REPORT, DeviceInfo.getInstance(context).getDeviceID(), new Gson().toJson(report));

				final String dataString = new Gson().toJson(data);

				new Thread() {
					public void run() {
						try {
							PushService.getServer().reply(InetAddress.getByName(ip), dataString);
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();

			}
		}

	}
}
