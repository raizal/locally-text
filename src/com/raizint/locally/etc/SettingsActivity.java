package com.raizint.locally.etc;

import io.fabric.sdk.android.Fabric;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.raizint.locally.R;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener{

	private SharedPreferences sp;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.pref_setting);
		setContentView(R.layout.preference_layout);
		init();	
		Fabric.with(this, new Crashlytics());
		AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sp, String key) {

		if (key.equals(Utils.NAME))
			findPreference(Utils.NAME).setSummary(sp.getString(Utils.NAME, ""));
		else if (key.equals(Utils.VISIBILITY)) {
			Utils.sendToUI("VISIBILITY : " + Utils.getSettingBooleanValue(this, Utils.VISIBILITY), this);
			if (PushService.getInstance() != null)
				PushService.getInstance().initNotification();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private void init() {		
		SharedPreferences sp = getPreferenceManager().getDefaultSharedPreferences(this);
		sp.registerOnSharedPreferenceChangeListener(this);
		findPreference(Utils.NAME).setSummary(sp.getString(Utils.NAME, ""));
		findPreference(Utils.UID).setSummary(sp.getString(Utils.UID, ""));
		CheckBoxPreference visibility = (CheckBoxPreference) findPreference(Utils.VISIBILITY);
		visibility.setChecked(Utils.getSettingBooleanValue(this, Utils.VISIBILITY));
		final Preference service = findPreference(Utils.SERVICE);

		if (Utils.getSettingBooleanValue(getBaseContext(), Utils.STATUS)) {
			service.setTitle("Stop Service");
			startService(new Intent(this,PushService.class));
		} else {
			service.setTitle("Start Service");
		}

		service.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				Utils.setBooleanSettingValue(getBaseContext(), Utils.STATUS, !Utils.getSettingBooleanValue(getBaseContext(), Utils.STATUS));
				if (Utils.getSettingBooleanValue(getBaseContext(), Utils.STATUS)) {
					startService(new Intent(getBaseContext(), PushService.class));
					service.setTitle("Stop Service");
				} else {
					stopService(new Intent(getBaseContext(), PushService.class));
					service.setTitle("Start Service");
				}
				return false;
			}

		});

	}

}