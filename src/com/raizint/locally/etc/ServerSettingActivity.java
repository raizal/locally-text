package com.raizint.locally.etc;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.raizint.locally.R;
import com.raizint.locally.db.DB;
public class ServerSettingActivity extends FragmentActivity {
	
	private ListView listview;
	
	private Adapter adapter = new Adapter();
	
	private ArrayList<PC> data;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.desktop_listview);
				
		refresh();
		listview = (ListView) findViewById(R.id.listview);
		listview.setAdapter(adapter);
		registerForContextMenu(listview);
	}
	
	private void refresh(){		
		data = DB.getInstance(this).getAllPermitted();
		adapter.notifyDataSetChanged();
	}	
	
	private class Adapter extends BaseAdapter{

		@Override
		public int getCount() {			
			return data.size();
		}

		@Override
		public Object getItem(int arg0) { 
			return data.get(arg0);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView==null)
				convertView = getLayoutInflater().inflate(R.layout.desktop_item, parent,false);
			
			PC item = (PC) getItem(position);
			String ipStr = DB.getInstance(ServerSettingActivity.this).lastIp(item.getUid());
			
			TextView name = (TextView) convertView.findViewById(R.id.name);
			TextView uid = (TextView) convertView.findViewById(R.id.uid);
			TextView ip = (TextView) convertView.findViewById(R.id.ip);
			
			name.setText(item.getName());
			uid.setText(item.getUid());
			ip.setText(ipStr);
			
			final String _uid = item.getUid();
			
			convertView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					new ConfirmationDialog(ServerSettingActivity.this, _uid).show();
				}
			});
			
			
			return convertView;
		}
		
	}
	
	private class ConfirmationDialog extends Dialog implements View.OnClickListener{

		private Button yes,no;
		private String uid;
		
		public ConfirmationDialog(Context context,String  uid) {
			super(context);
			setContentView(R.layout.delete_confirm_dialog);			
			setCancelable(true);
			setTitle(R.string.server_remove_confirm_title);
			
			yes = (Button) findViewById(R.id.yes);
			no = (Button) findViewById(R.id.no);
			yes.setOnClickListener(this);
			no.setOnClickListener(this);
			this.uid = uid;
		}

		@Override
		public void onClick(View v) {
			if(v==yes){
				DB.getInstance(ServerSettingActivity.this).removePermission(uid);
				refresh();
				dismiss();
			}else if(v==no){
				dismiss();
			}
		}
		
	}
	
}
