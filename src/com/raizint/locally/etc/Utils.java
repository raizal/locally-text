package com.raizint.locally.etc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;

public class Utils {
	private static Gson gson;

	public static Gson getGson() {
		if (gson == null)
			gson = new Gson();
		return gson;
	}

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String formatDate(Calendar cal) {
		return sdf.format(cal.getTime());
	}

	public static void sendToUI(String message, Context c) {
		Intent intent = new Intent("SERVICE.RESULT");
		intent.putExtra("text", message);
		Log.d("SendtoUI", message);
		c.sendBroadcast(intent);

		PrintWriter pw;
		try {
			File file = new File(Environment.getExternalStorageDirectory(), "log");
			if (!file.exists())
				file.createNewFile();

			pw = new PrintWriter(new FileWriter(file, true));
			pw.append(formatDate(Calendar.getInstance())+" : "+message + "\n");
			pw.flush();
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getLog() {

		File file = new File(Environment.getExternalStorageDirectory() + "/log");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "";
		}

		Scanner scanner;
		try {

			scanner = new Scanner(file);

			StringBuilder sb = new StringBuilder();

			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine() + "\n");
			}
			scanner.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static final String UID = "locally_uid";
	public static final String NAME = "locally_name";
	public static final String OS = "locally_os";
	public static final String MODEL = "locally_model";
	public static final String STATUS = "locally_status";
	public static final String SERVICE = "locally_service";
	public static final String VISIBILITY = "locally_visibility";

	public static SharedPreferences getSetting(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static String getSettingStringValue(Context context, String name) {
		return getSetting(context).getString(name, "");
	}

	public static boolean getSettingBooleanValue(Context context, String name) {
		return getSetting(context).getBoolean(name, false);
	}

	public static void setSettingValue(Context context, String name, String value) {
		SharedPreferences.Editor edit = getSetting(context).edit();
		edit.putString(name, value);
		edit.commit();
	}

	public static void setBooleanSettingValue(Context context, String name, boolean value) {
		SharedPreferences.Editor edit = getSetting(context).edit();
		edit.putBoolean(name, value);
		edit.commit();
	}

	public static String checkapi(Context context) {
		String Exp_string = "";
		try {
			Class pkgClass = null;
			pkgClass = Class.forName("android.telephony.MSimSmsManager");
			Method[] methods = pkgClass.getMethods();
			for (Method m : methods) {
				// if (m.getName().contains(str))
				{
					Exp_string = Exp_string + m.toGenericString() + "\n\n";
				}
			}

		} catch (IllegalArgumentException e) {
			Exp_string = "IllegalArgumentException";
		} catch (ClassNotFoundException e) {
			Exp_string = "ClassNotFoundException";
		} catch (SecurityException e) {
			Exp_string = "SecurityException";
		}
		return Exp_string;
	}

	public static String SendSMS(String phone, String msg, int phoneType, PendingIntent sent, PendingIntent delivered) {
		String Exp_string = "";
		try {
			Class smsManagerClass = null;
			Class[] sendTextMessageParams = { String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, int.class };

			Method sendTextMessage = null;

			smsManagerClass = Class.forName("android.telephony.MSimSmsManager");

			Method method = smsManagerClass.getMethod("getDefault", new Class[] {});

			Object smsManager = method.invoke(smsManagerClass, new Object[] {});

			sendTextMessage = smsManagerClass.getMethod("sendTextMessage", sendTextMessageParams);
			// sendTextMessage =
			// SmsManager.getDefault().getClass().getMethod("sendTextMessage",
			// sendTextMessageParams);

			sendTextMessage.invoke(SmsManager.getDefault(), phone, "", msg, sent, delivered, phoneType);

		} catch (IllegalArgumentException e) {
			Exp_string = "IllegalArgumentException";
		} catch (IllegalAccessException e) {
			Exp_string = "IllegalAccessException";
		} catch (InvocationTargetException e) {
			Exp_string = "InvocationTargetException";
		} catch (ClassNotFoundException e) {
			Exp_string = "ClassNotFoundException";
		} catch (SecurityException e) {
			Exp_string = "SecurityException";
		} catch (NoSuchMethodException e) {
			Exp_string = "NoSuchMethodException";
		} catch (Exception e) {
			Exp_string = e.getMessage();
		}
		return Exp_string;
	}

	public static String getISMS() {

		try {
			Class sm = Class.forName("android.os.ServiceManager");
			Class[] params = { String.class };

			Method method = sm.getMethod("getService", params);

			return method.invoke(sm, "isms").toString();

		} catch (Exception e) {
			return e.getMessage();
		}

	}

	public static String SendSMSMultiPart(String phone, String msg, int phoneType, ArrayList sent, ArrayList delivered) {
		String Exp_string = "";
		try {
			Class smsManagerClass = null;
			Class[] divideMessagePamas = { String.class };
			Class[] sendMultipartTextMessageParams = { String.class, String.class, ArrayList.class, ArrayList.class, ArrayList.class, int.class };
			Method divideMessage = null;

			Method sendMultipartTextMessage = null;

			smsManagerClass = Class.forName("android.telephony.MSimSmsManager");

			Method method = smsManagerClass.getMethod("getDefault", new Class[] {});

			Object smsManager = method.invoke(smsManagerClass, new Object[] {});

			divideMessage = smsManagerClass.getMethod("divideMessage", divideMessagePamas);

			sendMultipartTextMessage = smsManagerClass.getMethod("sendMultipartTextMessage", sendMultipartTextMessageParams);

			ArrayList magArray = (ArrayList) divideMessage.invoke(smsManager, msg);

			sendMultipartTextMessage.invoke(smsManager, phone, "", magArray, sent, delivered, phoneType);

		} catch (IllegalArgumentException e) {
			Exp_string = "IllegalArgumentException";
		} catch (IllegalAccessException e) {
			Exp_string = "IllegalAccessException";
		} catch (InvocationTargetException e) {
			Exp_string = "InvocationTargetException";
		} catch (ClassNotFoundException e) {
			Exp_string = "ClassNotFoundException";
		} catch (SecurityException e) {
			Exp_string = "SecurityException";
		} catch (NoSuchMethodException e) {
			Exp_string = "NoSuchMethodException";
		}
		return Exp_string;
	}

	public static String getPreferredSMSSubscription() {
		try {
			Class smsManagerClass = null;
			smsManagerClass = Class.forName("android.telephony.MSimSmsManager");

			Method method = smsManagerClass.getMethod("getDefault", new Class[] {});

			Object smsManager = method.invoke(smsManagerClass, new Object[] {});

			Method method2 = smsManagerClass.getMethod("getPreferredSmsSubscription");

			return method2.invoke(smsManager, new Object[] {}).toString();

		} catch (Exception e) {
			return e.getMessage();
		}

	}

	public static int getPhoneCount() {
		try {
			Class smsManagerClass = null;
			smsManagerClass = Class.forName("android.telephony.MSimTelephonyManager");

			Method method = smsManagerClass.getMethod("getDefault", new Class[] {});

			Object smsManager = method.invoke(smsManagerClass, new Object[] {});

			Method method2 = smsManagerClass.getMethod("getPhoneCount");

			int result = 1;
			try {
				result = Integer.parseInt(method2.invoke(smsManager, new Object[] {}).toString());
			} catch (Exception e) {
				result = 1;
			}
			return result;
		} catch (Exception e) {
			return 1;
		}
	}

	public static int SIM1 = 0;
	public static int SIM2 = 1;

	public static String setDefaultSubscription(int sim) {
		try {
			Class smsManagerClass = null;
			Class[] params = { int.class };
			smsManagerClass = Class.forName("com.android.internal.telephony.MSimPhoneFactory");
			Method method2 = smsManagerClass.getMethod("setDefaultSubscription", params);

			return method2.invoke(smsManagerClass, new Object[] { sim }).toString();

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static String getDefaultSubscription() {
		try {
			Class smsManagerClass = null;
			smsManagerClass = Class.forName("com.android.internal.telephony.MSimPhoneFactory");
			Method method2 = smsManagerClass.getMethod("getDefaultSubscription", new Class[] {});
			return method2.invoke(smsManagerClass, new Object[] {}).toString();

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static String checkapi(Context context, String target) {
		String Exp_string = "";
		try {
			Class pkgClass = null;
			pkgClass = Class.forName(target);
			Method[] methods = pkgClass.getMethods();
			for (Method m : methods) {
				{
					Exp_string = Exp_string + m.toGenericString() + "\n\n";
				}
			}

		} catch (IllegalArgumentException e) {
			Exp_string = "IllegalArgumentException";
		} catch (ClassNotFoundException e) {
			Exp_string = "ClassNotFoundException";
		} catch (SecurityException e) {
			Exp_string = "SecurityException";
		}
		return Exp_string;
	}

	public static String getNetworkOperatorName(int sim) {

		try {
			Class smsManagerClass = null;
			smsManagerClass = Class.forName("android.telephony.MSimTelephonyManager");
			Class[] params = new Class[] { int.class };

			Method method = smsManagerClass.getMethod("getDefault", new Class[] {});

			Object smsManager = method.invoke(smsManagerClass, new Object[] {});

			Method method2 = smsManagerClass.getMethod("getNetworkOperatorName", params);
			return method2.invoke(smsManager, new Object[] { sim }).toString();

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static String getNetworkOperatorName(Context context) {
		TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String carrierName = manager.getNetworkOperatorName();
		return carrierName;
	}

}
