package com.raizint.locally.etc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.raizint.locally.AuthActivity;
import com.raizint.locally.dao.DaoContact;
import com.raizint.locally.dao.DaoSMS;
import com.raizint.locally.dao.DeviceInfo;
import com.raizint.locally.db.DB;
import com.raizint.locally.shared.Commands;
import com.raizint.locally.shared.Constant;
import com.raizint.locally.shared.Contact;
import com.raizint.locally.shared.Data;
import com.raizint.locally.shared.SMS;
import com.raizint.raizsocket.etc.DiscoveryServerEvent;
import com.raizint.raizsocket.udp.DiscoveryServer;

public class PushService extends Service implements DiscoveryServerEvent {

	private static PushService instance;

	private static DiscoveryServer server;
	private static Thread thread;
	private static HashMap<String, ArrayList<String>> delayedMessage = new HashMap<String, ArrayList<String>>();
	private static Context context;

	private static final int NOTIF_ID = 31122983;

	public static void addDelayedMessage(String uid, String message) {
		if (delayedMessage != null && delayedMessage.containsKey(uid)) {
			ArrayList<String> data = delayedMessage.get(uid);
			if (data == null) {
				data = new ArrayList<String>();
				delayedMessage.put(uid, data);
			}

			data.add(message);
		}
	}

	public static ArrayList<String> getDelayedMessage(String uid) {
		if (delayedMessage.containsKey(uid)) {
			return delayedMessage.get(uid);
		}

		return null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public void initNotification() {
		final NotificationManager mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent intentForNotif = new Intent(this, SettingsActivity.class);
		intentForNotif.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pIntent = PendingIntent.getActivity(this, 921129, intentForNotif, PendingIntent.FLAG_UPDATE_CURRENT);

		Notification notif = new NotificationCompat.Builder(this).setSmallIcon(com.raizint.locally.R.drawable.ic_launcher).setContentTitle("Locally-Text is up now")
				.setContentText("Device is " + (Utils.getSettingBooleanValue(this, Utils.VISIBILITY) ? "visible on local network" : "hidden from local network")).setContentIntent(pIntent).getNotification();

		notif.flags = notif.FLAG_NO_CLEAR | notif.FLAG_ONGOING_EVENT;
		startForeground(NOTIF_ID, notif);
							
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		if(instance==null){		
			instance = this;
			Utils.sendToUI("INIT ALARM SERVICE", this);
			AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			PendingIntent pi = PendingIntent.getService(this,19092939, new Intent(this,PushService.class), PendingIntent.FLAG_UPDATE_CURRENT);
			alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),3*60*1000, pi);
			// (AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+2000, pi);
		}

		if (intent != null)
			context = this;
		
		initNotification();

		if (Utils.getSettingBooleanValue(context, Utils.STATUS)) {
			Utils.sendToUI("INIT SERVER", this);
			initServer();
			Utils.sendToUI("REFRESH SMS", this);
			refreshSMS();
		} else {
			try {
				if (server != null)
					server.getSocket().close();
			} catch (Exception e) {

			} finally {
				server = null;
			}
			final NotificationManager mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
			mgr.cancel(NOTIF_ID);
			
			Utils.sendToUI("REMOVING ALARM SERVICE", this);
			AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
			PendingIntent pi = PendingIntent.getService(this,19092939, new Intent(this,PushService.class), PendingIntent.FLAG_CANCEL_CURRENT);			
			alarmMgr.cancel(pi);
			// (AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+2000, pi);			
		}
		return Service.START_STICKY;
	}

	public String getSMS() {
		return null;
	}

	public String getContact() {
		return null;
	}

	public static DiscoveryServer getServer() {
		return server;
	}

	private void action(DatagramPacket receivedPacket) {
		try {
			for (String strData : DiscoveryServer.getData(receivedPacket)) {

				Data data = Utils.getGson().fromJson(strData, Data.class);
				Utils.sendToUI(strData, this);

				DB.getInstance(this).log(data.getUID(), data.getCommand(), System.currentTimeMillis(), receivedPacket.getAddress().getHostAddress());

				if (data.getCommand().equals(Commands.FIND_SERVER_SUCCESS)) {

					if (DB.getInstance(this).isPermitted(data.getUID()) || Utils.getSettingBooleanValue(this, Utils.VISIBILITY)) {
						server.reply(receivedPacket.getAddress(), Utils.getGson().toJson(new Data(Commands.FIND_DEVICE_SUCCESS, DeviceInfo.getInstance(this).getDeviceID(), Utils.getGson().toJson(DeviceInfo.getInstance(this)))));
					}

				} else if (data.getCommand().equals(Commands.CONNECT_AUTH) || data.getCommand().equals(Commands.CONNECT_AUTH_REJECTED)) {

					final NotificationManager mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

					Intent intent = new Intent(PushService.this, AuthActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtra("data", Utils.getGson().toJson(receivedPacket));
					PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

					Notification notif = new NotificationCompat.Builder(this).setDefaults(Notification.DEFAULT_ALL).setSmallIcon(com.raizint.locally.R.drawable.ic_launcher).setContentTitle("New Connection Request")
							.setContentText("A new Desktop is trying to connect to this device").setContentIntent(pIntent).getNotification();
					notif.flags = Notification.FLAG_AUTO_CANCEL;

					mgr.notify(0, notif);

				} else if (data.getCommand().equals(Commands.CONNECT_AUTH_ACCEPTED)) {

					DB.getInstance(this).givePermission(data.getUID(), data.getData());
					final Data tempData = data;
					final DatagramPacket tempPacket = receivedPacket;
					new Thread() {
						public void run() {
							sendSMSResponses(tempData, tempPacket);
						};
					}.start();
					new Thread() {
						public void run() {
							sendContact(tempPacket);
						};
					}.start();

				} else if (data.getCommand().equals(Commands.FIND_DEVICE_SUCCESS)) {
				} else if (data.getCommand().equals(Commands.DISCONNECT)) {

					DB.getInstance(this).removePermission(data.getUID());
					server.reply(receivedPacket.getAddress(), Utils.getGson().toJson(new Data(Commands.DISCONNECT_SUCCESS, DeviceInfo.getInstance(this).getDeviceID(), "")));

				} else if (data.getCommand().equals(Commands.CONNECT_PERMISION)) {
					Utils.sendToUI("Check Permission", this);
					String[] arrayData = null;
					if (!DB.getInstance(this).isPermitted(data.getUID())) {
						if (Utils.getSettingBooleanValue(context, Utils.VISIBILITY)) {
							arrayData = new String[] { Utils.getGson().toJson(new Data(Commands.CONNECT_REJECTED, DeviceInfo.getInstance(this).getDeviceID(), Utils.getGson().toJson(DeviceInfo.getInstance(this)))) };
							server.reply(receivedPacket.getAddress(), receivedPacket.getPort(), arrayData);
						}
					} else {
						arrayData = new String[] { Utils.getGson().toJson(new Data(Commands.CONNECT_SUCCESS, DeviceInfo.getInstance(this).getDeviceID(), Utils.getGson().toJson(DeviceInfo.getInstance(this)))) };
						server.reply(receivedPacket.getAddress(), receivedPacket.getPort(), arrayData);
					}
				} else {
					if (DB.getInstance(this).isPermitted(data.getUID())) {

						// SEND MESSAGE
						if (data.getCommand().equals(Commands.SMS_SEND)) {
							SMS sms = new Gson().fromJson(data.getData(), SMS.class);
							DaoSMS.sendMessage(this, sms.getSender(), sms.getMessage(), sms.getDatetime(), receivedPacket.getAddress().getHostAddress(), sms.getSim());
						}
						// MESSAGE SYNC
						else if (data.getCommand().equals(Commands.SYNC_SMS_REQ)) {
							Utils.sendToUI("SINKRONISASI SMS", this);
							sendSMSResponses(data, receivedPacket);
						}
						// CONTACT SYNC
						else if (data.getCommand().equals(Commands.SYNC_CONTACT_REQ)) {
							sendContact(receivedPacket);
						}
					} else {
						if (Utils.getSettingBooleanValue(context, Utils.VISIBILITY)) {
							Utils.sendToUI("REJECTED", this);
							String[] arrayData = new String[] { Utils.getGson().toJson(new Data(Commands.CONNECT_REJECTED, DeviceInfo.getInstance(this).getDeviceID(), Utils.getGson().toJson(DeviceInfo.getInstance(this)))) };

							server.reply(receivedPacket.getAddress(), Constant.PORT_UDP, arrayData);
						}
					}
					DB.getInstance(this).log(data.getUID(), data.getCommand(), Calendar.getInstance().getTimeInMillis(), receivedPacket.getAddress().getHostAddress());
				}
			}

		} catch (Exception e) {

		}
	}

	@Override
	public void onDestroy() {
		NotificationManager mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mgr.cancel(NOTIF_ID);
		Utils.sendToUI("UNREGISTER RECEIVER", this);
		super.onDestroy();
	}

	private void sendContact(DatagramPacket receivedPacket) {

		ArrayList<Contact> contactArray = DaoContact.getContact(this);
		ArrayList<Contact> tempContactContainer = new ArrayList<Contact>();
		ArrayList<String> dataArray = new ArrayList<String>();
		while (contactArray.size() > 0) {
			tempContactContainer.add(contactArray.remove(0));
			if (tempContactContainer.toString().getBytes().length >= 10000 || contactArray.size() == 0) {
				String contacts = new Gson().toJson(tempContactContainer);
				final Data respond2 = new Data(Commands.SYNC_CONTACT_RESP, DeviceInfo.getInstance(this).getDeviceID(), contacts);
				dataArray.add(new Gson().toJson(respond2));
				tempContactContainer.clear();
			}
		}
		server.reply(receivedPacket.getAddress(), Constant.PORT_UDP, dataArray.toArray(new String[dataArray.size()]));
	}

	public void sendSMS(SMS sms) {
		Utils.sendToUI("SENDING NEW SMS TO PC", this);
		if (DB.getInstance(this).getAllPermittedUID().size() > 0) {

			ArrayList<String> permitteds = DB.getInstance(this).getAllPermittedUID();
			final String data = new Gson().toJson(new Data(Commands.SMS_NEW, DeviceInfo.getInstance(this).getDeviceID(), new Gson().toJson(sms)));
			// Send one by one to permitted uid
			for (final String uid : permitteds) {
				new Thread() {
					public void run() {
						String ip = DB.getInstance(PushService.context).lastIp(uid);
						if (ping(uid, ip)) {							
							try {
								server.reply(InetAddress.getByName(ip), data);
								Utils.sendToUI(data, PushService.this);
							} catch (UnknownHostException e) {
								e.printStackTrace();
							}
						} else {
							PushService.addDelayedMessage(uid, data);							
						}
					};
				}.start();

			}
		}
	}

	private void refreshSMS() {
		ArrayList<String> permitteds = DB.getInstance(this).getAllPermittedUID();
		for (final String uid : permitteds) {

			new Thread() {
				public void run() {
					String ip = DB.getInstance(PushService.context).lastIp(uid);
					long lasttime = DB.getInstance(context).lastTimeLog(uid, Commands.SYNC_SMS_RESP);
					if (DaoSMS.countMessages(context, lasttime) > 0) {
						Utils.sendToUI("Ping~ " + ip, PushService.this);
						if (ping(uid, ip)) {
							try {
								Utils.sendToUI("Preparing message for "+ip , PushService.this);
								ArrayList<SMS> arraySms = DaoSMS.getMessages(context, lasttime);
								DB.getInstance(context).log(uid, Commands.SYNC_SMS_RESP, System.currentTimeMillis(), ip);

								ArrayList<SMS> tempArraySMS = new ArrayList<SMS>();

								if (arraySms.size() == 0) {
									String sms = new Gson().toJson(arraySms);
									String respond = new Gson().toJson(new Data(Commands.SYNC_SMS_RESP, DeviceInfo.getInstance(context).getDeviceID(), sms));
									server.reply(InetAddress.getByName(ip), respond);
									return;
								}
								Utils.sendToUI("Sending data to "+ip, PushService.this);
								while (arraySms.size() > 0) {

									tempArraySMS.add(arraySms.remove(0));
									if (tempArraySMS.toString().getBytes().length >= 10000 || arraySms.size() == 0) {
										String sms = new Gson().toJson(tempArraySMS);
										String respond = new Gson().toJson(new Data(Commands.SYNC_SMS_RESP, DeviceInfo.getInstance(context).getDeviceID(), sms));
										server.reply(InetAddress.getByName(ip), respond);
										tempArraySMS.clear();
									}
								}

							} catch (UnknownHostException e) {
								e.printStackTrace();
							}
						}else{
							Utils.sendToUI("Ping~ " + ip+" failed", PushService.this);
						}
					}
				};
			}.start();

		}
	}

	private void sendSMSResponses(Data data, DatagramPacket receivedPacket) {
		
		try {
			long lasttime = DB.getInstance(this).lastTimeLog(data.getUID(), Commands.SYNC_SMS_RESP);
			Utils.sendToUI("SMS REQ LAST TIME LOG : " + String.valueOf(lasttime), this);
			ArrayList<SMS> arraySms = DaoSMS.getMessages(this, lasttime);

			Utils.sendToUI("SMS COUNT : " + arraySms.size(), this);

			DB.getInstance(this).log(data.getUID(), Commands.SYNC_SMS_RESP, System.currentTimeMillis(), receivedPacket.getAddress().getHostAddress());

			ArrayList<SMS> tempArraySMS = new ArrayList<SMS>();

			if (arraySms.size() == 0) {
				String sms = new Gson().toJson(arraySms);
				String respond = new Gson().toJson(new Data(Commands.SYNC_SMS_RESP, DeviceInfo.getInstance(this).getDeviceID(), sms));
				server.reply(receivedPacket.getAddress(), respond);
				return;
			}

			while (arraySms.size() > 0) {

				tempArraySMS.add(arraySms.remove(0));
				if (tempArraySMS.toString().getBytes().length >= 10000 || arraySms.size() == 0) {
					String sms = new Gson().toJson(tempArraySMS);
					String respond = new Gson().toJson(new Data(Commands.SYNC_SMS_RESP, DeviceInfo.getInstance(this).getDeviceID(), sms));
					server.reply(receivedPacket.getAddress(), respond);
					Log.d("DATA TO SEND", "DTS : " + respond.getBytes().length);
					tempArraySMS.clear();
				}
			}
		} catch (Exception e) {
			Utils.sendToUI(e.getMessage(), this);
		}

	}

	public boolean ping(String uid, String address) {
		String message = new Gson().toJson(new Data(Commands.PING_PC, DeviceInfo.getInstance(this).getDeviceID(), null));
		DatagramSocket socket = null;
		try {
			DatagramPacket packet = DiscoveryServer.preparePacket(new String[] { message }, InetAddress.getByName(address), Constant.PORT_UDP);
			socket = new DatagramSocket();
			socket.setSoTimeout(5000);
			socket.send(packet);
			byte[] recvBuf = new byte[1024 * 8];
			DatagramPacket retreivedPacket = new DatagramPacket(recvBuf, recvBuf.length);
			socket.receive(retreivedPacket);

			for (String dataStr : DiscoveryServer.getData(retreivedPacket)) {
				Data data = new Gson().fromJson(dataStr, Data.class);
				socket.close();
				return data.getCommand().equals(Commands.PING_PC_RESULT) && data.getUID().equals(uid);
			}

		} catch (IOException e) {
			return false;
		} finally {
			if (socket != null)
				socket.close();
		}
		return false;
	}

	@Override
	public void onReply(DatagramPacket packet) {
		Utils.sendToUI(DiscoveryServer.getData(packet).toString(), PushService.this);
	}

	@Override
	public void onReceive(DatagramPacket receivedPacket) {
		final DatagramPacket packet = receivedPacket;
		new Thread() {
			@Override
			public void run() {
				action(packet);
			}
		}.start();
	}

	@Override
	public void onOpen() {
		Log.d(getClass().getPackage().toString(), "Discovery Service OPEN");
		Utils.sendToUI("Discovery Service OPEN", PushService.this);
	}

	@Override
	public void onClose() {
		Utils.sendToUI("Discovery Service Closed", PushService.this);
		server = null;
		thread = null;
	}

	public static PushService getInstance() {
		return instance;
	}

	private void initServer() {
		if (server == null || server.getSocket().isClosed()) {
			try {
				server = new DiscoveryServer(Constant.PORT_UDP, PushService.this);
			} catch (SocketException e1) {
				e1.printStackTrace();
			}
		}

		if (thread == null || !thread.isAlive()) {
			Utils.sendToUI("DiscoveryService's Running", PushService.this);

			thread = new Thread() {
				public void run() {
					try {
						server.open();
					} catch (Exception e) {
						Utils.sendToUI(e.getMessage(), PushService.this);
						e.printStackTrace();
					}
					Utils.sendToUI("DiscoveryService's Stopped", PushService.this);
				};
			};
			thread.setDaemon(true);
			thread.start();
		} else {
			Utils.sendToUI("DiscoveryService's Already Running", this);
		}
	}
	
	@Override
	public void onLowMemory() {		
		NotificationManager mgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mgr.cancel(NOTIF_ID);
		Utils.sendToUI("ON LOW MEMORY UNREGISTER RECEIVER", this);
		super.onLowMemory();
	}
	
}
