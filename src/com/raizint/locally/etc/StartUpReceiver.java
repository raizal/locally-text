package com.raizint.locally.etc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartUpReceiver extends BroadcastReceiver {
	public StartUpReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (Utils.getSettingBooleanValue(context, Utils.STATUS))
			context.startService(new Intent(context,PushService.class));	
	}
}
