package com.raizint.locally.db;

import android.net.Uri;

public class Constant {
	
	public static final String Inbox = "content://sms/inbox"
	,Failed = "content://sms/failed"
	,Queued = "content://sms/queued"
	,Sent = "content://sms/sent"
	,Draft = "content://sms/draft"
	,Outbox = "content://sms/outbox"
	,Undelivered = "content://sms/undelivered";
		
	public static final Uri SMS_SENT = Uri.parse("content://sms/sent");
	
	public static final String DB_PERMITTED = "permitted";
	public static final String DB_PERMITTED_UID = "uid";
	public static final String DB_PERMITTED_NAME = "name";
	public static final String DB_PERMITTED_IP = "ip";
	
	public static final String DB_LOG = "log";
	public static final String DB_LOG_ID = "id";
	public static final String DB_LOG_UID = "uid";
	public static final String DB_LOG_COMMAND= "command";
	public static final String DB_LOG_TIME= "time";
	public static final String DB_LOG_IP = "ip";
	
	public static final String DB_Device = "device";
	public static final String DB_Device_ID = "id";		
	
}


