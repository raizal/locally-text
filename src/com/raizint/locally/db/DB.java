package com.raizint.locally.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.raizint.locally.dao.DeviceInfo;
import com.raizint.locally.etc.PC;
import com.raizint.locally.etc.Utils;

public class DB extends SQLiteOpenHelper {

	private static DB instance;

	public static DB getInstance(Context context) {
		if(instance==null)
			instance = new DB(context);
		return instance;
	}

	private final static String DB_NAME = "locally.db";
	private final static int VERSION = 1;

	private Context context;

	public DB(Context context) {
		super(context, DB_NAME, null, VERSION);
		this.context = context;
		Utils.sendToUI("Database instance created", context);	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		DeviceInfo.getInstance(context);
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(context.getAssets().open("db.sql"));

			String buf = "";

			while (scan.hasNextLine()) {
				buf += scan.nextLine();
			}

			String[] q = buf.split(";");

			for (String sql : q) {
				db.execSQL(sql);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public SQLiteDatabase getDatabase() {
		return getWritableDatabase();
	}

	public boolean isPermitted(String uid) {
		SQLiteDatabase db = getWritableDatabase();
		Cursor c = db.rawQuery("select * from " + Constant.DB_PERMITTED + " where " + Constant.DB_PERMITTED_UID + "=\""
				+ uid + "\"", null);		
		boolean result = c.moveToFirst();
		c.close();
		return result;
	}

	public void removePermission(String uid) {
		SQLiteDatabase db = getWritableDatabase();
		db.delete(Constant.DB_PERMITTED, Constant.DB_PERMITTED_UID+"=\""+uid+"\"",null);
		db.delete(Constant.DB_LOG,Constant.DB_LOG_UID+"=\""+uid+"\"", null);
	}	
	
	public void givePermission(String uid) {
		SQLiteDatabase db = getWritableDatabase();

		Cursor c = db.rawQuery("select * from " + Constant.DB_PERMITTED + " where " + Constant.DB_PERMITTED_UID + "=\""
				+ uid + "\"", null);

		if (!c.moveToFirst()) {
			ContentValues values = new ContentValues();
			values.put(Constant.DB_PERMITTED_UID, uid);
			db.insert(Constant.DB_PERMITTED, null, values);
		}
		c.close();

	}
	
	public void givePermission(String uid,String name) {
		SQLiteDatabase db = getWritableDatabase();

		Cursor c = db.rawQuery("select * from " + Constant.DB_PERMITTED + " where " + Constant.DB_PERMITTED_UID + "=\""
				+ uid + "\"", null);

		if (!c.moveToFirst()) {
			ContentValues values = new ContentValues();
			values.put(Constant.DB_PERMITTED_UID, uid);
			values.put(Constant.DB_PERMITTED_NAME, name);
			db.insert(Constant.DB_PERMITTED, null, values);
		}
		c.close();

	}	
	
	public void updatePermittedData(String uid,String name,String ip){
		SQLiteDatabase db = getWritableDatabase();

		Cursor c = db.rawQuery("select * from " + Constant.DB_PERMITTED + " where " + Constant.DB_PERMITTED_UID + "=\""
				+ uid + "\"", null);

		if (c.moveToFirst()) {
			ContentValues values = new ContentValues();			
			values.put(Constant.DB_PERMITTED_NAME, name);
			db.update(Constant.DB_PERMITTED, values,Constant.DB_PERMITTED_UID+"=\""+uid+"\"",null);
		}
		c.close();		
	}
	
	public ArrayList<PC> getAllPermitted(){
		SQLiteDatabase db = getWritableDatabase();		
		Cursor c = db.rawQuery("select * from permitted", null);
		ArrayList<PC> result = new ArrayList<PC>();
		if(c.moveToFirst()){			
			do{
				result.add(new PC(c.getString(c.getColumnIndex("name")), c.getString(c.getColumnIndex("uid"))));
			}while(c.moveToNext());			
		}
		c.close();
		return result;
	}
	
	public String lastIp(String uid) {
		String res = null;
		SQLiteDatabase db = getWritableDatabase();

		Cursor c = db.rawQuery("select ip,MAX(time) from log where uid=\"" + uid + "\"", null);
		if (!c.isClosed() && c.moveToFirst()) {
			res = c.getString(0);
		}
		c.close();
		return res;
	}

	public void log(String uid, String command, long time, String ip) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.putNull(Constant.DB_LOG_ID);
		values.put(Constant.DB_LOG_COMMAND, command);
		values.put(Constant.DB_LOG_UID, uid);
		values.put(Constant.DB_LOG_IP, ip);
		values.put(Constant.DB_LOG_TIME, String.valueOf(time));
		db.insert(Constant.DB_LOG, null, values);

	}

	public long lastTimeLog(String uid, String command) {
		long result = 0;
		SQLiteDatabase db = getReadableDatabase();
		try {
			Cursor c = db.rawQuery("select MAX(CAST(" + Constant.DB_LOG_TIME + " as INTEGER)) from " + Constant.DB_LOG
					+ "" + " where " + Constant.DB_LOG_UID + "=\"" + uid + "\" " + "and " + Constant.DB_LOG_COMMAND
					+ "=\"" + command + "\"", null);
			if (c.moveToFirst())
				result = c.getLong(0);
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<String> getAllPermittedUID() {
		ArrayList<String> result = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.rawQuery("select * from " + Constant.DB_PERMITTED, null);

		if (c.moveToFirst()) {
			do {
				result.add(c.getString(0));
			} while (c.moveToNext());
			c.close();
		}
		c.close();
		
		return result;
	}

}
