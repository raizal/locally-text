package com.raizint.locally;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.raizint.locally.etc.SettingsActivity;
import com.raizint.locally.etc.Utils;

public class MainActivity extends FragmentActivity {

	protected static final String TAG = "MainActivity";
	private static MainActivity instance;

	private ViewPager viewPager;

	private ViewPagerFragment f1 = new ViewPagerFragment();

	public static MainActivity getInstance() {
		return instance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		instance = this;
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		viewPager.setAdapter(adapter);		

		Intent intent = new Intent("com.raizint.locally.startup");
		sendBroadcast(intent);
	}

	FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

		@Override
		public int getCount() {
			return 1;
		}

		@Override
		public Fragment getItem(int pos) {
			switch (pos) {
			case 0:
				return f1;
			case 1:
				return new ViewPagerFragment();
			}

			return null;
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
