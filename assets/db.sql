CREATE TABLE "permitted" (
"uid" TEXT PRIMARY KEY,
"name" TEXT
);

CREATE TABLE "log"(
"id" INTEGER PRIMARY KEY AUTOINCREMENT,
"uid" TEXT,
"command" TEXT,
"desc" TEXT,
"time"  INTEGER,
"ip"  TEXT
);

CREATE TABLE "device"(
"id" TEXT,
"name" TEXT,
"version" TEXT
);